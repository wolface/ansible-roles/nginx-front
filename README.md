nginx-front
=========

Simple configuration for nginx as a front-end, initially to put in front of uwsgi for Django

Requirements
------------

None

Role Variables
--------------


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

WTFPL

Author Information
------------------

Written by [Gilou](https://gilouweb.com) from [Wolface.IT](https://www.wolface.fr)
